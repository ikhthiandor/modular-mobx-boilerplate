

export const baseURL = 'http://128.199.205.225/'


// export const assets = {
//   ccc: require('../public/CCC_Logo_BlackBG.svg'),
//   zap: require('../public/icons/zap.svg'),
//   arya: require('../public/icons/arya.svg'),
//   aryaWhite: require('../public/icons/arya-white.svg'),
//   primaryLogo: require('../public/CobaltLogo.svg'),
//   primaryLogoWhite: require('../public/CobaltLogoWhite.svg'),
// }

export const colors = {
  activeSecondaryNav: '#e2e2e3',
  backgroundGray: '#f5f7fa', // CONTENT AREA
  gray: '#FAFBFC', //ACTIONSHEET, TITLEBAR, FOOTER
  lightGray: '#dddddd',
  lightBlue: '#001f38',
  lighterGray: '#f8f8f8',
  darkGray: '#888888',
  dark: '#001224',
  lightDark: '#001A33',
  white: '#FFFFFF',
  red: '#900020',
  lightRed: '#730020',
}

export const fonts = {
  primary: 'Quicksand',
  secondary: 'Quicksand',
  code: 'Quicksand',
  numeric: 'Quicksand',
  handwriting: 'Quicksand',
  normalSize: '1rem',
  large: '1.2rem',
  larger: '1.6rem',
  small: '0.6rem',
  fat: 700,
}

export const mixins = {
  makeFlex: (direction, justifyContent, alignItems, alignContent, flexWrap) => ({
    display: 'flex',
    flexDirection: direction || 'row',
    justifyContent: justifyContent || 'center',
    alignItems: alignItems || 'center',
    alignContent: alignContent || 'stretch',
  }),
  stretch: () => ({
    alignSelf: 'stretch',
    flex: 1,
  }),
  dimensions: (width='50%', height='50%') => ({
    width: width,
    height: height,
    minWidth: width,
    maxWidth: width,
    minHeight: height,
    maxHeight: height,
  }),
  fullScreen: (absolute=true) => ({
    height: absolute ? '100vh' :'100%',
    width: absolute ? '100vw' : '100%',
  }),
  rounded: (topRight, bottomRight, bottomLeft, topLeft) => {
    return {
      borderTopRightRadius: topRight,
      borderBottomRightRadius: bottomRight,
      borderBottomLeftRadius: bottomLeft,
      borderTopLeftRadius: topLeft,
    }
  },
  defaultInput: (color, borderRadius) => ({
    width: '100%',
    color: color,
    borderRadius: borderRadius || 22,
    border: `2px solid ${color || 'black'}`,
    outline: 'none',
    fontSize: '1rem',
    padding: '10px 5px',
    margin: '4px 0',
    paddingLeft: 10,
    fontFamily: 'Quicksand',
    textAlign: 'center',
  }),
  actionSheetButton: {
    button: {
      margin: '0 10px',
      background: colors.dark,
      color: colors.white,
      fontFamily: fonts.primary,
      textTransform: 'uppercase',
      borderRadius: 22,
      width: 200,
      padding: 10,
      border: 'none',
      cursor: 'pointer',
      outline: 'none',
      fontSize: '0.6rem',
    },
  },
}


export function makePair(props) {
  let output = []
  for (let i of Object.keys(props)) {
    output.push(i + '=' + props[i])
  }

  return output.join('&')
}


export function makeUrl(base, props) {
  if (base.includes('?')) {
    return `${base}&${makePair(props)}`
  } else {
    return `${base}?${makePair(props)}`
  }
}

export const modalStyles = (width, height, top, left) => ({
  overlay: {
    background: 'rgba(0, 0, 0, 0.7)',
  },
  content: {
    position: 'absolute',
    background: 'transparend',
    border: 'none',
    top: top || `${50-height/2}%`,
    left: left || `${50-width/2}%`,
    marginRight: '-50%',
    height: `${height}rem` || '50vh',
    width: `${width}rem` || '50vw',
    ...mixins.makeFlex(),
  },
})
