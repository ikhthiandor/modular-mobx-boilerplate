import React, {Component} from 'react'
import {StyleSheet, css} from 'aphrodite'

import {observer, inject} from 'mobx-react'
import * as C from '../../config/config'

export default class ProjectStatusComponent extends Component {
    render() {

        let S = StyleSheet.create({
            card: {

                ...C.mixins.makeFlex('row'),
                borderRadius: '22px',
                fontFamily: C.fonts.primary,
                border: '1px solid #eee',
                ...C.mixins.dimensions('80vw', '70px')

            },
            image: {
                ...C.mixins.dimensions('10vw', '100%'),
                ...C.mixins.makeFlex('column'),
                //border: '1px solid red',
                padding: 5

            },
            info: {
                ...C.mixins.dimensions('20vw', '100%'),
                ...C.mixins.makeFlex('column', 'flex-start', 'flex-start'),
                //border: '1px solid red',
                padding: 5

            },
            projectname: {
              margin: 10,
              fontSize: C.fonts.large,
              color: C.colors.dark
            },

            description: {
              fontSize: C.fonts.small,
              color: C.colors.lightGray

            },

            time: {
                ...C.mixins.dimensions('20vw', '100%'),
                ...C.mixins.makeFlex('column', 'flex-start'),
                //border: '1px solid red',
                padding: 5
            },
            lastchange: {
              marginTop: 10
            },

            human: {
              fontSize: C.fonts.small,
              color: C.colors.lightGray
            },

            numchanges: {
                //border: '1px solid red',
                padding: 5,
                color: C.colors.lightGray,
                ...C.mixins.makeFlex(),
                ...C.mixins.dimensions('10vw', '100%'),

            },
            numfiles: {
                //border: '1px solid red',
                padding: 5,
                color: C.colors.lightGray,
                ...C.mixins.makeFlex(),
                ...C.mixins.dimensions('10vw', '100%'),
            },
            percentage: {
                //border: '1px solid red',
                padding: 5,
                ...C.mixins.makeFlex(),
                ...C.mixins.dimensions('10vw', '100%'),

            }
        })

        return (
            <div className={css(S.card)}>
                <div className={css(S.image)}>Image</div>
                <div className={css(S.info)}>

                    <div className={css(S.projectname)}>Project X</div>
                    <div className={css(S.description)}>The quick brown fox jumps over</div>

                </div>

                <div className={css(S.time)}>
                    <div className={css(S.lastchange)}>Last Change</div>
                    <div className={css(S.human)}>Yesterday at 10:20 pm</div>

                </div>

                <div className={css(S.numchanges)}>15</div>
                <div className={css(S.numfiles)}>480</div>
                <div className={css(S.percentage)}>50%</div>

            </div>

        )
    }
}
