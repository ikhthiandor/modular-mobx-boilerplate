import React, {Component} from 'react'
import {StyleSheet, css} from 'aphrodite'

import {observer, inject} from 'mobx-react'
import * as C from '../../config/config'



export default class CardDemo extends Component {
  render() {

    let S = StyleSheet.create({
      card: {

        ...C.mixins.makeFlex('row'),
        borderRadius: '22px',
        ...C.mixins.dimensions('400px', '150px')

      },

      icon: {
        flex:1,
        backgroundColor: '#564AA3',
        height: '100%',
        ...C.mixins.rounded(0, 0, 22, 22),
        ...C.mixins.makeFlex()

      },
      info: {
        flex: 2,
        height: '100%',
        backgroundColor: '#7266BA',
        color: C.colors.white,
        ...C.mixins.rounded(22, 22, 0, 0),
        ...C.mixins.makeFlex('column'),
      }

    })

    return (
      <div className={css(S.card)}>
        <div className={css(S.icon)}>Foo</div>
        <div className={css(S.info)}>

          <div className={css(S.value)}>700GB</div>
          <div className={css(S.label)}>QUOTA</div>
        </div>

      </div>
    )
  }
}
