import React from 'react'
import {Route, IndexRoute} from 'react-router'

// import CardDemo from './components/CardDemo'
import ProjectStatusComponent from './components/ProjectStatusComponent'

const angleDemoRoutes = (
  // <Route path="/carddemo" component={CardDemo}>
  //   <IndexRoute component={CardDemo} />
  // </Route>


  <Route path="/status" component={ProjectStatusComponent}>
    <IndexRoute component={ProjectStatusComponent} />
  </Route>

)

export default angleDemoRoutes
