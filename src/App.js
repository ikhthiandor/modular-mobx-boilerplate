import React, {Component} from 'react'
import {Router, browserHistory} from 'react-router'
import {Provider} from 'mobx-react'

import baseRoutes from './base/routes'

/* ------ COUNTER APP --- --- */
import {counterModel} from './counter/models/counter'
import counterRoutes from './counter/routes'
import angleDemoRoutes  from './angle/routes'

const stores = {
  /*------ Initiate the Stores ------ */
  counterModel,
}

export default class App extends Component {
  render() {
    return (
      <Provider {...stores}>
        <Router history={browserHistory}>
          {/* Other routes should come before base routes */}
          {counterRoutes}
          {angleDemoRoutes}
          {baseRoutes}
        </Router>
      </Provider>
    )
  }
}
